package com.company;

public class Main {
    public static  void  main(String[] args){
        Kereta komuter = new Kereta();
        komuter.tambahTiket("Ahmad Phyton");
        komuter.tambahTiket("Junaedi Kotlin");
        komuter.tambahTiket("Saiful HTML");
        komuter.tampilkanTiket();

        Kereta KAJJ = new Kereta("Jayabaya",2);
        KAJJ.tambahTiket("Vania Malinda","Malinda","Surabaya Gubeng");
        KAJJ.tambahTiket("Sekar SD","Malang","Sidoarjo");
        KAJJ.tambahTiket("Bonaventura","Malang","Surabaya Pasarturi");
        KAJJ.tampilkanTiket();
    }
}