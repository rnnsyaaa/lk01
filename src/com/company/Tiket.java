package com.company;

public class Tiket {
    private String namaPenumpang;
    private  String Asal;
    private String Tujuan;


    public Tiket(String namaPenumpang) {
        this.namaPenumpang= namaPenumpang;
    }

    public Tiket(String namaPenumpang,String Asal,String Tujuan ){
       this.namaPenumpang=namaPenumpang;
       this.Asal=Asal;
       this.Tujuan=Tujuan;
    }

    public String getNamaPenumpang(){
        return namaPenumpang;
    }

    public String getAsal() {
        return Asal;
    }

    public String getTujuan() {
        return Tujuan;
    }
}
