package com.company;
import java.util.ArrayList;

public class Kereta {
    private String jenisKereta;
    private int tiketTerjual;
    private int batasTiket;
    private ArrayList<Tiket> daftarTiket;


    //Default Constructor part
    public  Kereta(){
        jenisKereta = "Komuter";
        batasTiket =1000;
       daftarTiket = new ArrayList<Tiket>();
    }

    //Overload Constructor part
    public Kereta (String jenisKereta,int batasTiket){
    this.jenisKereta= jenisKereta;
    this.batasTiket=batasTiket;
    this.daftarTiket= new ArrayList<Tiket>();
    }

    //Method untuk menambah tiket
    public void tambahTiket(String namaPenumpang){
    Tiket tiket = new Tiket(namaPenumpang);
    if (jenisKereta.equals("Komuter")) {
        daftarTiket.add(new Tiket(namaPenumpang));
    } else {
        System.out.println("Pemesanan tiket untuk KAJJ Membutuhkan asal dan tujuan");
    }
    tiketTerjual++;
        if (batasTiket-tiketTerjual >= 30){
            System.out.println("==============================================");
            System.out.println("Tiket Berhasil Dipesan");
        }
        else if(batasTiket - tiketTerjual < 30){
            System.out.println("==============================================");
            System.out.println("Maaf Tiket Habis Terjual");
                    }
        else {
            System.out.println("==============================================");
            System.out.println("Tiket Berhasil Dipesan. Sisa Tiket Tersedia :  " +(batasTiket-tiketTerjual));

        }

    }

    public void tambahTiket(String namaPenumpang,String Asal,String Tujuan){

        if (daftarTiket.size() < batasTiket ){
            if (jenisKereta.equals("Komuter")) {
                System.out.println("Pemesanan tiket Komuter tidak membutuhkan Asal dan Tujuan");
            } else {
                daftarTiket.add(new Tiket(namaPenumpang, Asal, Tujuan));
                tiketTerjual++;

                if (batasTiket - tiketTerjual >= 30) {
                    System.out.println("==============================================");
                    System.out.println("Tiket Berhasil Dipesan");
                } else if (batasTiket - tiketTerjual < 0) {
                    System.out.println("==============================================");
                    System.out.println("Mohon Maaf Tiket Habis Terjual");
                } else {
                    System.out.println("==============================================");
                    System.out.println("Tiket Berhasil Dipesan. Sisa Tiket Tersedia :  " +(batasTiket-tiketTerjual));
                    int habis = batasTiket - tiketTerjual - 1;
                    if (habis < 0){
                        System.out.println("==============================================");
                        System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
                    }
                }
            }
        }
    }


    //Method untuk menampilkan tiket
    public void tampilkanTiket() {
        System.out.println("===============================================");
        System.out.println("Daftar penumpang Kereta Api " + jenisKereta + ":");
        System.out.println("-------------------------------");
        if (tiketTerjual == 0) {
            System.out.println("Belum ada tiket yang terjual");
        } else {
            for (int i = 0; i < tiketTerjual; i++) {
                    System.out.println("Nama    : " + daftarTiket.get(i).getNamaPenumpang());
                if (daftarTiket.get(i).getAsal() != null) {
                    System.out.println("Asal    : " + daftarTiket.get(i).getAsal());
                    System.out.println("Tujuan  : " + daftarTiket.get(i).getTujuan());
                    System.out.println("-------------------------------");
                }
            }
        }
        System.out.println("\n");

    }

    }


